/* ########################## Stepper Config ########################## */
// Define pin connections & motor's steps per revolution
#define DIR_PIN 14 
#define STEP_PIN 12 
#define SLEEP_PIN 5
#define RESET_PIN 4
#define ENABELE_PIN 13 
#define stepsPerRevolution 200
#define microsteppingFactor 16


void setup_stepper(){
  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
  digitalWrite(DIR_PIN, HIGH);
  pinMode(SLEEP_PIN, OUTPUT);
  digitalWrite(SLEEP_PIN, LOW);
  pinMode(ENABELE_PIN, OUTPUT);
  digitalWrite(ENABELE_PIN, HIGH);
  pinMode(RESET_PIN, OUTPUT);
  digitalWrite(RESET_PIN, LOW);
  delay(1);
  digitalWrite(RESET_PIN, HIGH);
  delay(1);
}

void stepper_one_turn(){
  digitalWrite(SLEEP_PIN, HIGH);
  delay(1);
  digitalWrite(ENABELE_PIN, LOW);
  delay(1);
  for(int x = 0; x < stepsPerRevolution * microsteppingFactor; x++)
  {
    //Serial.println(x);
    digitalWrite(STEP_PIN, HIGH);
    //delayMicroseconds(1000);
    delay(1);
    digitalWrite(STEP_PIN, LOW);
    delay(1);
    //delayMicroseconds(1000);
  }
  //Serial.println("Turn Finished");
  digitalWrite(ENABELE_PIN, HIGH);
  digitalWrite(SLEEP_PIN, LOW);
}

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println("Starting");
  delay(10);
  setup_stepper();
  stepper_one_turn();
}

void loop() {
  // put your main code here, to run repeatedly:

}
