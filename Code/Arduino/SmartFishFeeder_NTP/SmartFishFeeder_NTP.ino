/* ########################## includes ########################## */
#include <NTPClient.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <WiFiUdp.h>
#include <EEPROM.h>
#include "WiFiAutoSelector.h"

#define DEBUG 1

/* ########################## Fish Feeder Setup ########################## */
//#define sleep_interval 1*60E6 //1 minute in microseconds, max: 71 minutes
//#define feed_interval 10*60 //10 minutes in seconds

#define sleep_interval 60*60E6 //60 minutes in microseconds, max: 71 minutes
#define feed_interval 24*60*60 //24 hours in seconds
#define max_on_time 60000 //1 minute in millis

/* ########################## Stepper Config ########################## */
// Define pin connections & motor's steps per revolution
#define dirPin D1 //= 2;
#define stepPin D2 //= 5;
#define sleepPin D5 //= 13;
#define stepsPerRevolution 200
#define microsteppingFactor 16

/* ########################## WIFI ########################## */
#define WIFI_CONNECT_TIMEOUT 8000
WiFiAutoSelector wifiAutoSelector(WIFI_CONNECT_TIMEOUT);

/* ########################## MQTT ########################## */
#define mqtt_send_retries 10
WiFiClient wifi_client;
const char* mqtt_server = "192.168.179.11";
const char* my_hostname = "SmartFishFeeder";
PubSubClient client(mqtt_server, 1883, wifi_client); // 1883 is the listener port for the Broker
const char* mqtt_watchdog_topic = "watchdog_fish_feeder";
const char* mqtt_send_topic = "status_fish_feeder";
const char* mqtt_receive_topic = "feed_fish_now";

/* ########################## NTP ########################## */
const long utcOffsetInSeconds = 3600;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);

/* ########################## EEPROM ########################## */

uint8_t check_time_diff(uint32_t timestamp_now, uint32_t last_timestamp, int debug = false) {
  if (debug == true)
  {
    Serial.print(last_timestamp); // one day: 86400s
    Serial.print(" ");
    Serial.print(timestamp_now);
    Serial.print(" ");
    Serial.println(timestamp_now - last_timestamp);
  }
  if (timestamp_now >= last_timestamp + feed_interval)
  {
    if (debug == true)
    {
      Serial.print("Updating EEPROM");
    }
    write_timestamp(timestamp_now);
    return true;
  }
  else {
    return false;
  }
}

void check_ontime_status() {
  if (millis() > max_on_time)
  {
    ESP.restart();
  }
}

void setup() {
  Serial.begin(9600);
  Serial.println();
  Serial.println("Starting");
  delay(10);
  setup_stepper();
  setup_wifi();
  timeClient.begin();
  delay(1);
  timeClient.update();
  uint32_t last_timestamp = read_last_timestamp();
  uint32_t timestamp_now = timeClient.getEpochTime();
  setup_mqtt();
  sendMQTTint(timestamp_now, mqtt_watchdog_topic); //watchdog on every wakeup
  if (check_time_diff(timestamp_now, last_timestamp) == true) {
    Serial.println("We should feed the fish...");
    stepper_one_turn();
    sendMQTTint(timestamp_now, mqtt_send_topic);
  }
  Serial.print("Going to sleep for ");
  delay(10);
  Serial.print(sleep_interval);
  delay(10);
  Serial.println(" microseconds");
  delay(100);
  ESP.deepSleep(sleep_interval);
  delay(100);
}

void loop() {
}
