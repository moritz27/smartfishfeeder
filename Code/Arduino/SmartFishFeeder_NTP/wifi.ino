//void setup_wifi(void) {
//  #include "credentials.h"
//  wifiAutoSelector.scanAndConnect();
//  //Serial.println(WiFi.localIP());
//}

void setup_wifi(void) {
  #include "credentials.h"
  
  WiFi.mode(WIFI_STA);

  WiFi.begin(ssid, wifi_password);

  Serial.println("Setup WIFI");

  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    WiFi.begin(ssid, wifi_password);
    Serial.println("Retrying connection...");
    check_ontime_status();
  }
  // Wait until the connection has been confirmed before continuing
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    check_ontime_status();
  }
  Serial.println();
  // Debugging - Output the IP Address of the ESP8266
  Serial.println("WiFi connected");
  delay(10);
  Serial.print("IP address: ");
  delay(10);
  Serial.println(WiFi.localIP());
  delay(10);
}
