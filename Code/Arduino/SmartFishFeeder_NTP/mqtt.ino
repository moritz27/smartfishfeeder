void setup_mqtt(void) {
  Serial.println("Setup MQTT");
  // Connect to MQTT Broker
  // client.connect returns a boolean value to let us know if the connection was successful.
  client.setCallback(ReceivedMessage);
  while (!client.connected()) {
    Connect();
    delay(500);
    Serial.print(".");
    check_ontime_status();
  }
  Serial.println();
  delay(1000);
  client.subscribe(mqtt_receive_topic);
}

bool Connect() {
  if (client.connect(my_hostname)) {
    return true;
  }
  else {
    return false;
  }
}

void sendMQTTint(int content, const char* topic) {
  char stringToSend[10];
  sprintf(stringToSend, "%i", content);
  sendMQTTstring(stringToSend, topic);
}

void sendMQTTstring(const char* content, const char* topic) {
  for(int i=0; i<= mqtt_send_retries; i++){
    if (client.publish(topic, content)) {
      Serial.print(topic);  Serial.print(" ");  Serial.print(content); Serial.println(" message sent");
      break;
    }
    else{
      Connect();
      delay(10);
    }
  }
}

void ReceivedMessage(char* topic, byte* payload, unsigned int length) {
//  Serial.print("Received new MQTT message with a length of: ");
//  Serial.println(length);
//  for (int i=0; i <= length-1; i++){
//  Serial.print((char)payload[i]);
//  }
  if ((char)payload[0] == '1') {
    Serial.println("Feed Da Fish!");
    stepper_one_turn();
  }
}
