void setup_stepper(){
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  digitalWrite(dirPin, HIGH);
  pinMode(sleepPin, OUTPUT);
  digitalWrite(sleepPin, LOW);
}

void stepper_one_turn(){
  digitalWrite(sleepPin, HIGH);
  delay(1);
  for(int x = 0; x < stepsPerRevolution * microsteppingFactor; x++)
  {
    //Serial.println(x);
    digitalWrite(stepPin, HIGH);
    //delayMicroseconds(1000);
    delay(1);
    digitalWrite(stepPin, LOW);
    delay(1);
    //delayMicroseconds(1000);
  }
  //Serial.println("Turn Finished");
  digitalWrite(sleepPin, LOW);
}
