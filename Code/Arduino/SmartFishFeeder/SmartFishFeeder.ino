/* ########################## includes ########################## */
#include <ESP8266WiFi.h> // Enables the ESP8266 to connect to the local network (via WiFi)
#include <PubSubClient.h> // Allows us to connect to, and publish to the MQTT broker
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>


/* ########################## Stepper Config ########################## */
// Define pin connections & motor's steps per revolution
const int dirPin = D1;//= 2;
const int stepPin = D2;//= 5;
const int sleepPin = D3;//= 13;
const int stepsPerRevolution = 200;
const int microsteppingFactor = 16;

/* ########################## WIFI ########################## */
//const char* ssid = "SchneW";
//const char* wifi_password = "alleskleinundzusammen";
const char* ssid = "EasyBox-286034";
const char* wifi_password = "NadineMoritz022018";
WiFiClient wifi_client;

/* ########################## MQTT ########################## */
const char* mqtt_server = "192.168.179.11";
const char* my_hostname = "SmartFishFeeder";
PubSubClient client(mqtt_server, 1883, wifi_client); // 1883 is the listener port for the Broker
const char* mqtt_receive_topic = "feed_fish_now";

/* ########################## startup code ########################## */
void setup() {
  Serial.begin(115200);
  setup_stepper();
  setup_wfi();
  setup_mqtt();
  setup_ota();
}

/* ########################## loop ########################## */

void loop() {
  if (!client.connected()) {
    ConnectReceive();
  }
  client.loop();
  ArduinoOTA.handle();
}
