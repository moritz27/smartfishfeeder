void setup_mqtt(void) {
  Serial.println("Setup MQTT");
  // Connect to MQTT Broker
  // client.connect returns a boolean value to let us know if the connection was successful.
  client.setCallback(ReceivedMessage);
  while (!client.connected()) {
    ConnectReceive();
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  delay(1000); 
}


bool ConnectReceive() {
  // Connect to MQTT Server and subscribe to the topic
  if (client.connect(my_hostname)) {
      client.subscribe(mqtt_receive_topic);
      return true;
    }
    else {
      return false;
  }
}

void ReceivedMessage(char* topic, byte* payload, unsigned int length) {
//  Serial.print("Received new MQTT message with a length of: ");
//  Serial.println(length);
//  for (int i=0; i <= length-1; i++){
//  Serial.print((char)payload[i]);
//  }
  if ((char)payload[0] == '1') {
    Serial.println("Feed Da Fish!");
    stepper_one_turn();
  }
}
