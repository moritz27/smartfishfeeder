#include "bme_sensor.h"

Adafruit_BME280 bme; // I2C

void setup_bme(const uint8_t sda_pin, const uint8_t scl_pin){
    Wire.begin(sda_pin, scl_pin);
    bme.begin(0x76);
}

void read_bme_to_json(DynamicJsonDocument& sensor_values){
    sensor_values["pressure"] = bme.readPressure();
    sensor_values["temperature"] = bme.readTemperature();
    sensor_values["humidity"] = bme.readHumidity();
}