#include "mqtt.h"

uint8_t setup_mqtt(PubSubClient& client, const char* hostname, const uint8_t retries, bool debug) {
  Serial.println("Setup MQTT");
  uint8_t success = false;
  for (int i = 0; i <= retries; i++) {
    client.connect(hostname);
    if (client.connected()) {
      success = true;
      break;
    }
    if (success == true && debug == true) {
      Serial.println("MQTT connected");
    }
    else if (debug == true) {
      Serial.println("MQTT could not be connected");
    }
  }
  return success;
}

uint32_t send_mqtt_string(PubSubClient& client, const char* hostname, const char* content, const char* topic, const uint8_t retries, bool debug) {
  if (!client.connected()) {
    if (debug == true){
      Serial.println("MQTT disconnected, reconnect now");
    }
    client.connect(hostname);
    delay(100);
  }
  int success = false;
  for (int i = 0; i <= retries; i++) {
    if (debug == true){
      Serial.print("trying to send topic: ");
      Serial.println(topic);
    }
    if (client.publish(topic, content)) {
      success = true;
      break;
    }
    else {
      client.connect(hostname);
      delay(100);
    }
    delay(100);
  }
  if (success == true) {
    if (debug == true){
      Serial.print(topic); Serial.println(" message sent");
    }
    return micros();
  }
  else {
    if (debug == true){
      Serial.print(topic); Serial.println(" message could not be sent, restarting now");
    }
    delay(100);
    ESP.restart();
    delay(100);
    return 0;
  }
}

uint32_t send_mqtt_json(PubSubClient& client, const char* hostname, DynamicJsonDocument& json, const char* topic, const uint8_t retries, bool debug) {
    String json_string;
    serializeJson(json, json_string);
    return send_mqtt_string(client, hostname, json_string.c_str(), topic);
}

