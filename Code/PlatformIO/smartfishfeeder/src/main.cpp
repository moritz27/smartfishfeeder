#include <Arduino.h>
#include <ArduinoJson.h>
#include "config.h"
#include "my_functions.h"
#include "wifi.h"
#include "mqtt.h"
#include "ntp.h"
#include "eeprom_timestamp.h"
#include "stepper_motor.h"

WiFiClient wifiClient;
PubSubClient client(mqtt_server, 1883, wifiClient); // 1883 is the listener port for the Broker
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);

/* ########################## startup code ########################## */
void setup() {
  delay(10);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN,HIGH);
  if (DEBUG == true){
    Serial.begin(115200);
    Serial.println();
  }
  setup_stepper();
  setup_wifi(wifiClient, ssid, wifi_password, DEBUG);
  uint8_t mqtt_connected = setup_mqtt(client, my_hostname, DEBUG);
  setup_ntp_client(timeClient);
  delay(10);
  DynamicJsonDocument doc(1024);
  uint32_t last_ts = read_last_timestamp();
  uint32_t now_ts = timeClient.getEpochTime();
  doc["last_feed_timestamp"] = last_ts;
  doc["now_timestamp"] = now_ts;
  doc["delta_timestamp"] = (now_ts - last_ts); 
  if(check_time_diff(now_ts, last_ts, FEED_INTERVAL, DEBUG)){
    stepper_one_turn(DEBUG); 
    write_timestamp(now_ts);
    doc["delta_timestamp"] = 0; 
  }
  if(mqtt_connected){
    uint32_t send_timestamp = send_mqtt_json(client, my_hostname, doc, mqtt_send_topic, DEBUG);
    digitalWrite(LED_PIN,LOW);
    dynamic_sleep(SLEEP_INTERVAL, send_timestamp, DEBUG);
  }
  else{
    dynamic_sleep(SLEEP_INTERVAL, micros(), DEBUG);
  }
}

/* ########################## loop ########################## */
void loop() {
}