#include "my_functions.h"

void dynamic_sleep(const uint32_t sleep_interval, uint32_t send_timestamp, bool debug){
  if(send_timestamp >= sleep_interval){
    if (debug == true)
    {
      Serial.print("Restarting ESP directly");
    }
    delay(10);
    Serial.flush();
    ESP.restart();
  }
  else{
    uint32_t sleep_time = sleep_interval - send_timestamp;
    if (debug == true)
    {
      Serial.print("Going to sleep for ");
      delay(10);
      Serial.print(sleep_time);
      delay(10);
      Serial.println(" microseconds");
      Serial.flush();
    }
    delay(10);
    #if defined(ESP8266)
      ESP.deepSleep(sleep_time);
    #elif defined(ESP32)
      esp_sleep_enable_timer_wakeup(sleep_time);
      esp_deep_sleep_start();
    #endif
    delay(10);
  }

}

uint8_t check_time_diff(uint32_t timestamp_now, uint32_t last_timestamp, const uint32_t feed_interval, bool debug) {
  if (debug == true)
  {
    Serial.print(last_timestamp); // one day: 86400s
    Serial.print(" ");
    Serial.print(timestamp_now);
    Serial.print(" ");
    Serial.print(timestamp_now - last_timestamp);
    Serial.print(" ");
    Serial.println(timestamp_now >= ((last_timestamp + feed_interval) - int(feed_interval/20)));
  }
  if (timestamp_now >= ((last_timestamp + feed_interval) - int(feed_interval/10)))
  {
    if (debug == true)
    {
      Serial.println("Updating EEPROM");
    }
    //write_timestamp(timestamp_now);
    return true;
  }
  else if (timestamp_now < last_timestamp){
    if (debug == true)
    {
      Serial.println("New Timestamp is not valid, restarting!");
    }
    delay(10);
    Serial.flush();
    ESP.restart();
    return false;
  }
  else {
    return false;
  }
}
