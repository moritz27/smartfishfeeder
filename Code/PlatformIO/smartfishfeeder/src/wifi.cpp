#include "wifi.h"

void setup_wifi(WiFiClient& wifi_client, const char* ssid, const char* wifi_password, bool debug, const uint8_t retries) {
  WiFi.mode(WIFI_STA);
  //WiFi.setSleepMode(WIFI_NONE_SLEEP);
  //WiFi.begin(ssid, wifi_password);
  int success = false;
  if (debug == true){
    Serial.println("Setup WIFI");
  }
  for (int i = 0; i <= retries; i++) {
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
      WiFi.begin(ssid, wifi_password);
      delay(200);
    }
    if (WiFi.status() == WL_CONNECTED) {
      success = true;
      break;
    }
  }
  if (success == true) {
    // Debugging - Output the IP Address of the ESP8266
    if (debug == true){
      Serial.print("WiFi connected, ");
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
    }
  }
  else {
    if (debug == true){
      Serial.println("WIFI could not be connected, restarting now");
    }
    delay(100);
    ESP.restart();
    delay(100);
  }
  return;
}