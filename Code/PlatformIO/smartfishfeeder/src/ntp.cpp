#include "ntp.h"

void setup_ntp_client(NTPClient& timeClient){
    timeClient.begin();
    delay(1);
    timeClient.update();
}