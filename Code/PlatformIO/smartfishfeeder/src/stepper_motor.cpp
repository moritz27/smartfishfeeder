#include "stepper_motor.h"

void setup_stepper(){
  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
  digitalWrite(DIR_PIN, HIGH);
  pinMode(SLEEP_PIN, OUTPUT);
  digitalWrite(SLEEP_PIN, LOW);
  pinMode(ENABELE_PIN, OUTPUT);
  digitalWrite(ENABELE_PIN, HIGH);
  pinMode(RESET_PIN, OUTPUT);
  digitalWrite(RESET_PIN, LOW);
  delay(1);
  digitalWrite(RESET_PIN, HIGH);
  delay(1);
}

void stepper_one_turn(bool debug){
  digitalWrite(SLEEP_PIN, HIGH);
  delay(1);
  digitalWrite(ENABELE_PIN, LOW);
  delay(1);
  if(debug == true){
    Serial.println("feed the fish!");
  }
  for(int x = 0; x < stepsPerRevolution * microsteppingFactor; x++)
  {
    //Serial.println(x);
    digitalWrite(STEP_PIN, HIGH);
    //delayMicroseconds(1000);
    delay(1);
    digitalWrite(STEP_PIN, LOW);
    delay(1);
    //delayMicroseconds(1000);
  }
  //Serial.println("Turn Finished");
  digitalWrite(ENABELE_PIN, HIGH);
  digitalWrite(SLEEP_PIN, LOW);
}