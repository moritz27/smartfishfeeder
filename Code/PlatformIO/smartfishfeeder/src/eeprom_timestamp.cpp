#include "eeprom_timestamp.h"

uint32_t read_last_timestamp(){
  EEPROM.begin(4);
  uint32_t ts = 0;
  for (int i = 0; i<=3; i++){
    ts += EEPROM.read(i) << (8*i);
  }
  EEPROM.end(); 
  return ts;
}

void write_timestamp(uint32_t ts){
  EEPROM.begin(4);
  for (int i = 0; i<=3; i++){
    EEPROM.write(i, (ts >> (8*i))&0xFF);
  }
  EEPROM.commit();
  EEPROM.end(); 
  return;
}