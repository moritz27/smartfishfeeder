#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <ArduinoJson.h>

void setup_bme(const uint8_t sda_pin, const uint8_t scl_pin);
void read_bme_to_json(DynamicJsonDocument& sensor_values);