#include <PubSubClient.h>
#include <ArduinoJson.h> 

uint8_t setup_mqtt(PubSubClient& client, const char* hostname, const uint8_t retries = 5, bool debug = false);
uint32_t send_mqtt_string(PubSubClient& client, const char* hostname, const char* content, const char* topic, const uint8_t retries = 5, bool debug = false);
uint32_t send_mqtt_json(PubSubClient& client, const char* hostname, DynamicJsonDocument& json, const char* topic, const uint8_t retries = 5, bool debug = false);
