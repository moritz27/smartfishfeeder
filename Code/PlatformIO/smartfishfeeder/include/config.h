#include "credentials.h"

#define DEBUG false

/* ########################## PINS ########################## */
#define LED_PIN 15
#define BUTTON_PIN 2

#if defined(ESP8266)
  #define SDA_PIN D14
  #define SCL_PIN D15
#elif defined(ESP32)
  #define SDA_PIN 21 // 16 //
  #define SCL_PIN 22 // 17 //
#endif

/* ########################## MQTT ########################## */
const char* mqtt_server = "192.168.179.11";
const char* mqtt_send_topic = "status_fish_feeder";
const char* mqtt_receive_topic = "feed_fish_now";
const char* my_hostname = "SmartFishFeeder";

/* ########################## NTP ########################## */
#define utcOffsetInSeconds 0


/* ########################## user variables ########################## */
#if DEBUG == true
  #define SLEEP_INTERVAL 30E6 //10 seconds in microseconds, max: 71 minutes
  #define FEED_INTERVAL 1*60 //1 minute in seconds
#else
  #define SLEEP_INTERVAL 30*60E6 //30 minutes in microseconds, max: 71 minutes
  #define FEED_INTERVAL 24*60*60 //24 hours in seconds
#endif



#define RETRY_NUM 5