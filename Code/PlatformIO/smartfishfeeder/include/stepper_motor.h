#include <Arduino.h>

#define DIR_PIN 14 
#define STEP_PIN 12 
#define SLEEP_PIN 5
#define RESET_PIN 4
#define ENABELE_PIN 13 
#define stepsPerRevolution 200
#define microsteppingFactor 16


void setup_stepper();
void stepper_one_turn(bool debug = false);