#include <Arduino.h>

void dynamic_sleep(const uint32_t sleep_interval, uint32_t send_timestamp = 0, bool debug = false);
uint8_t check_time_diff(uint32_t timestamp_now, uint32_t last_timestamp, const uint32_t feed_interval, bool debug = false);