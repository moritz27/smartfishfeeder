#if defined(ESP8266)
  #include <ESP8266WiFi.h>
#elif defined(ESP32)
  #include <WiFi.h>
#else
  #error "This ain't a ESP8266 or ESP32, dumbo!"
#endif

void setup_wifi(WiFiClient& wifi_client, const char* ssid, const char* wifi_password, bool debug = false, const uint8_t retries = 5);